/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * Quy dinh luat choi cua cac kieu choi
 * @author thanh
 */
public abstract class GameController{
    protected int maxPlayer;
    protected int number;
    
    public int getMaxPlayer(){
        return maxPlayer;
    }
    
    public int getNumber(){
        return number;
    }
    
    /**
     * Co` chua' gia' tri voi kha nang ket thuc tran dau
     * @return true, false tuy tung quy dinh cu the
     */
    public abstract boolean isDone();
    
    /**
     * Cap nhat trang thai cua controller sau moi van
     */
    public abstract void update();
    
    /**
     * Cai lai trang thai ban dau cua game
     */
    public abstract void reset();
    
    /**
     * Mo ta game
     * @return game context
     */
    public abstract String gameDes();
}
