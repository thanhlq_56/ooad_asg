/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

/**
 * Lop chua cac tien ich - In, ghi vao file...
 *
 * @author thanh
 */
public final class Utils {

    /**
     * Ghi lai lua chon cua nguoi choi de lay can cu cho viec tinh nuoc di
     *
     * @param ls
     */
    public static void writePlayersChoice(List<Player> ls) {
        try {
            BufferedWriter ps = new BufferedWriter(new FileWriter(fsrc, true));
            for (Player p : ls) {
                if (!p.isAuto()) {
                    for (Object o : p.getTableResult().get(PlayerResult.CHOICESTR)) {
                        ps.append((Integer) o + " ");
                    }
                }
            }
            ps.flush();
            ps.close();
        } catch (Exception e) {
            System.err.println("Cant save the choices!");
        }
    }

    /**
     * Hien thi bang ket qua
     *
     * @param ls
     */
    public static void showPlayersResult(List<Player> ls) {
        System.out.println(LINESTR + RESULTSTR);

        if (ls == null) {
            System.out.println("Null result");
            return;
        }
//        for (Player p : ls) {
//            System.out.printf("%-10s", p.getName());
//            p.getTableResult().showChoice();
//        }
        for (Player p : ls) {
            System.out.printf("%-10s", p.getName());
            p.getTableResult().showScore();
        }

        try {
            if (ls.get(ls.size() - 1).getTableResult().getScore()
                    > ls.get(ls.size() - 2).getTableResult().getScore()) {
                System.out.println(ls.get(ls.size() - 1).getName() + " win!");
            } else {
                if ( ls.size() == 2 )
                    System.out.println("Equal!");
            }
        } catch (NullPointerException | IndexOutOfBoundsException e) {
        }

        System.out.println(LINESTR + LINESTR);
    }

    /**
     * Tim ra tran dau nguoi choi tham gia
     *
     * @param p
     * @return Tran dau dang tham gia
     */
    public static Match scanPlayer(Player p) {
        for (Match m : Match.allMatch) {
            for (Player pl : m.getPlayers()) {
                if (p == pl) {
                    return m;
                }
            }
        }
        return null;
    }

    /**
     * Thong tin tran dau
     */
    public static void gameInfo() {
        System.out.println(LINESTR + INFOSTR);
        System.out.println(LINESTR + LINESTR);
    }

    public static void showHelp() {
        System.out.println(LINESTR + HELPSTR);
        System.out.println("\t'r' is Result");
        System.out.println("\t'h' is Help");
        System.out.println("\t'i' is Info");
        System.out.println("\t't' is reseT");
        System.out.println("\t'e' is Exit");
        System.out.println("\t_pick:");
        for (int i = 0; i < 3; i++) {
            System.out.printf("%10d %-10s\n", (i + 3), Choice.parseInt(i));
        }
        System.out.println(LINESTR + LINESTR);
    }

    /**
     * Reset tran dau: -- Goi ham reset tai match
     *
     * @param p
     */
    public static void reset(Player p) {
        scanPlayer(p).reset();
        System.out.println("The match has been reset");
    }

    /**
     * Chuan bi buoc vao tran dau: - In ra chi dan & dung 3s de nguoi choi chuan
     * bi buoc vao tran dau
     *
     * @param g
     */
    public static void prepare(GameController g) {
        System.out.println("Preparing...");
        System.out.println(g.gameDes());
        showHelp();
        try {
            Thread.sleep(3000); // Cho 3s de doc huong dan
        } catch (InterruptedException e) {
        }
        System.out.println("Game start!");
    }
    private static final String HELPSTR = "GUIDE";
    private static final String INFOSTR = "INFO";
    private static final String RESULTSTR = "RESULT";
    private static final String LINESTR = "_______________";
    public static final String GAMEKIND[]
            = {
                NRoundController.GAMEDES,
                DeathMatch.GAMEDES,
                ReachNPoint.GAMEDES
            };

    public static String fsrc = System.getProperty("user.dir") + "/result.dat";
}
