/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.gui;

import game.Choice;
import game.Player;
import game.PlayerResult;

/**
 *
 */
public class PlayerComponents {

    private Player player;
    private ChoiceComponents choiceComponents;
    private PlayerStC plstc;
    private boolean inTurn;
    
    // neu action dang hoat dong
    private boolean act;
    public PlayerComponents(Player p) {
        player = p;
    }

    public PlayerComponents() {
    }

    public void pickNext() {
        choiceComponents.next();
    }

    public void lockPicker() {

        assert ( choiceComponents != null );
        assert ( player != null );
        
        
        player.setHand(Choice.parseInt(
                choiceComponents.lock()));
        player.wakeUp();
    }
    
    public void changeFace(){
    }
    
    public void changeName(String s){
        plstc.setName(s);
        player.setName(s);
    }
    
    public void disable(){
        choiceComponents.setEnabled(false);
    }
    
    public void enable(){
        choiceComponents.setEnabled(true);
    }

    // -----SET ________ GET ----- //
    public void setChoiceComponents(ChoiceComponents c) {
        choiceComponents = c;
    }
    
    public void setPlayer(Player p){
        player = p;
        plstc.setName(p.getName());
    }
    
    public void setPickIcon(int ic){
        choiceComponents.setMainIcon(ic);
    }
    
    public void setPlayerStc(PlayerStC ps) {
        plstc = ps;
    }
    
    public boolean inTurn(){
        return !choiceComponents.lock;
    }
    
    // chua can public
    private ChoiceComponents getChoiceComponents() {
        return choiceComponents;
    }

    public Player getPlayer() {
        return player;
    }
    
    public boolean likeAuto(){
        return player.isAuto();
    }
    
    // 
    public void active(){
        choiceComponents.unlock();
    }
    
    public int getWins(){
        return player.getTableResult().getWins();
    }
    
    public PlayerResult resultTable(){
        return player.getTableResult();
    }
    
}
