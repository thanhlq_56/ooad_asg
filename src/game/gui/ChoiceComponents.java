/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.gui;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 */
public class ChoiceComponents extends JLabel {

    ImageIcon icon;
    List<JLabel> child;
    int count = 0;
    int maxImage = iconPath.length;
    boolean lock;

    public ChoiceComponents() {

        icons = new ArrayList<>();
        lock = false;
        initIcons();
        this.setIcon(icons.get(0));
    }

    public void addChild(JLabel c1, JLabel c2, JLabel c3) {
        if (child == null || child.isEmpty()) {
            child = new ArrayList<>();
        }

        child.add(c1);
        child.add(c2);
        child.add(c3);
    }

    /**
     * Thay doi icon cua label chinh toi icon ngay sau trong day iconPath
     */
    public void next() {
        if (lock) {
            return;
        }
        count = ++count % maxImage;
        setMainIcon(count);
    }

    /**
     * Icon cua o lon' nhat' ( o chon. )
     *
     * @param ic
     */
    public void setMainIcon(int ic) {
        setIcon(icons.get(ic));
    }

    @Override
    public void setIcon(Icon icon) {
        if (!this.isEnabled()) {
            return;
        }
        super.setIcon(icon); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Khoa lua chon cua icon va dua ra chi so lua chon
     *
     * @return lua chon tuong ung voi icon hien thi
     */
    public int lock() {

        for (int i = child.size() - 1; i > 0; --i) {
            try {
                child.get(i).setIcon(child.get(i - 1).getIcon());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Image ii = ((ImageIcon) this.getIcon())
                .getImage().getScaledInstance(child.get(0).getWidth(), child.get(0).getHeight(), Image.SCALE_SMOOTH);
        Icon ic = new ImageIcon(ii);
        child.get(0).setIcon((Icon) ic);
        lock = true;
        return count % 3;
    }

    @Override
    public void setEnabled(boolean stat) {
        super.setEnabled(stat);
        child.forEach((q) -> {
            q.setEnabled(stat);
        });
    }

    public void unlock() {
        lock = false;
    }

    public static final String iconPath[] = {
        "/images/rock.png",
        "/images/paper.png",
        "/images/scissors.png"
    };

    public static List<ImageIcon> icons = new ArrayList<>();

    public static void initIcons() {
        for (String p : iconPath) {
            try{
                System.out.println(p);
            icons.add(new ImageIcon( Class.forName("game.gui.MainFrame").getResource(p) ));
            } catch (ClassNotFoundException e){
                System.out.println("notFound");
            }
        }
    }
}
