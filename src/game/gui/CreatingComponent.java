/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.gui;

import game.Player;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

/**
 *
 * @author thanh
 */
final class CreatingComponent {

    public CreatingComponent(JTextField tf, JCheckBox cb) {
        this.cb = cb;
        this.tf = tf;
    }
    private JTextField tf;
    private JCheckBox cb;

    // Wrong
    public boolean isAuto() {
        return cb.isSelected();
    }
    
    

    /**
     * Kiem tra tinh hop le cua du lieu nhap vao khi tao nguoi choi
     * @return
     */
    public boolean isValid() {
        return (tf.getText().equals("") || tf.getText() == null);
    }

    public String getPlayerName() {
        return tf.getText();
    }

    public Player createPlayer() {
        Player pl = new Player(getPlayerName(), isAuto());
        return pl;
    }
}
