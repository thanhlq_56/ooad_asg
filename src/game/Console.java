/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Scanner;

/**
 * Nguoi choi thao tac, giao tiep voi game
 *
 * @author thanh
 */
public final class Console {

    /**
     * Lay lua chon cua nguoi choi -h: help -i: info -r: result -t: reset Match
     *
     * Number: lua chon de dau'
     *
     * @param p Player dang co luot
     * @return lua chon
     */
    public static void getInPut(Player p) {
        System.out.printf("%10s chose:", p.getName());
        p.setHand(
                Choice.parseInt(getChoiceNumber(p))
        );
        p.wakeUp();
    }

    private static int getChoiceNumber(Player p) {
        while (true) {
            String line = inp.nextLine();

            try {
                return Integer.parseInt(line);
            } catch (NumberFormatException e) {
                if (line.startsWith("e")) {
                    Utils.scanPlayer(p).setStat(Match.TERMINATED);
                    break;
                }
                if (line.startsWith("h")) {
                    Utils.showHelp();
                    continue;
                }
                if (line.startsWith("i")) {
                    Utils.gameInfo();
                    continue;
                }
                if (line.startsWith("r")) {
                    Utils.showPlayersResult(Utils.scanPlayer(p).getPlayers());
                    continue;
                }
                if (line.startsWith("t")) {
                    Utils.reset(p);
                    continue;
                }
            }
        }
        return 0;
    }

    /**
     * Tao luat choi tu input
     *
     * @return
     */
    public static GameController createRule() {
        GameController result = new NRoundController(0, 0);

        for (int i = 0; i < Utils.GAMEKIND.length; i++) {
            System.out.printf("%2d. %-30s\n", (i + 1), Utils.GAMEKIND[i]);
        }

        System.out.print("Pick a game: ");
        int p;

        do {
            try {
                p = Console.inp.nextInt();
                switch (p) {
                    case 1:
                        return NRoundController.createByConsole();
                    default:
                        System.out.println("Try again...");
                }
            } catch (Exception e) {
                continue;
            }
        } while (true);
    }

    public boolean inConsole; // Dung cho console va GUI
    public static final Scanner inp = new Scanner(System.in);
}
