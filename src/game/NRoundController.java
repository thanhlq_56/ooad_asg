/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * 
 * Luat choi: Don gian nhu dan ro
 * N nguoi choi choi den het N tran
 * 
 * @author thanh
 */
public class NRoundController extends GameController {

    private int roundth;

    public NRoundController(int n, int p) {
        number = n;
        maxPlayer = p;
    }

    public static NRoundController createByConsole() {
        int n, p;
        System.out.print("Max round?");
        n = Console.inp.nextInt();
        n = (n > 0) ? n : 0;

        System.out.print("Max player?");
        p = Console.inp.nextInt();
        p = (p > 0) ? p : 0;

        System.out.println("Created successfully!");
        return new NRoundController(n, p);
    }

    @Override
    public boolean isDone() {
        if ( roundth < number) {
            System.out.printf("\n____ROUND:%2s__\n", (roundth + 1) + "");
            return false;
        } else 
            return true;
    }
    @Override
    public void update() {
        roundth++;
    }

    public int getRoundth() {
        return roundth;
    }

    @Override
    public void reset() {
        roundth = 0;
    }

    public String gameDes() {
        if (maxPlayer == 0 && number == 0) {
            return GAMEDES;
        } else {
            return GAMEDES + "\n"
                    + maxPlayer + " players\t"
                    + number + " rounds";
        }
    }
    
    public void printInfo(){
        System.out.println("number: " + number);
        System.out.println("player: " + maxPlayer);
    }

    public static final String GAMEDES = "Play game for an arbitrary number of rounds";
}
