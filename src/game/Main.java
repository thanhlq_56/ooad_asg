package game;

import game.gui.MainFrame;


/**
 * 
 * Kich hoat
 * @author thanh
 */
public class Main {
    
    
    public static void main(String[] args) {
//        
        NRoundController nrc = (NRoundController)Console.createRule();
        Match m = Match.controllerCreate(nrc);
        m.addPlayer(Player.createByConsole());
        m.fillPlayer();
        m.setStat(Match.RUNNING);
        m.startGame();
        Utils.showPlayersResult(m.getPlayers());
//        MainFrame.main(args);
    }
    
    
}
