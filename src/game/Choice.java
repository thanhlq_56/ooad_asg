/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * Lop lua chon
 *
 * @author thanh
 */
public enum Choice {
    
    PAPER, ROCK, SCISSORS, UNKNOWN;

    /**
     * Lay lua chon tu so
     *
     * @param n
     * @return 1 BUA, 2 KEO, 3 VAI
     */
    public static Choice parseInt(int n) {
        switch (n % 3) {
            case 0:
                return PAPER;
            case 1:
                return ROCK;
            case 2:
                return SCISSORS;
            default: // UNKNOWN
                return UNKNOWN;
        }
    }

    /**
     * Tra ve ket qua danh bai lua chon
     *
     * @param n
     * @return
     */
    public static int enemyInt(int n) {
        return (n + 2) % 3;
    }

    /**
     * Xet xem hai lua chon co danh duoc nhau hay khong
     *
     * @param own
     * @param target
     * @return true neu own == enemy(target) false truong hop con lai
     */
    public static boolean beat(int own, int target) {
        return (own % 3 == enemyInt(target));
    }
    
    class ChoiceException extends Exception {

        public ChoiceException(String s) {
            super(s);
        }
        
    }
    
    public static final int LOSE = -1;
    public static final int EQUAL = 0;
    public static final int WIN = 1;

}
