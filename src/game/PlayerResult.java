/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Luu ket qua cua nguoi choi
 *
 * @author thanh
 */
public class PlayerResult extends HashMap<String, List> {

    private final String name;
    private int score;
    private int wins;

    public PlayerResult(Player p) {
        this.put(SCSTR, new ArrayList()); // It nhat phai co score
        this.put(CHOICESTR, new ArrayList());
        name = p.getName();
    }

    /**
     * Cap nhat ket qua sau moi hiep dau - ket thuc 1 round
     *
     * @param rs
     * @param c
     */
    synchronized public void updateRS(int rs, Choice c) {
        this.get(SCSTR).add(rs);
        this.get(CHOICESTR).add(c.ordinal());
        score += computeScore(rs);
        if (rs == Choice.WIN) {
            ++wins;
            System.out.println(wins + " wins");
        }
        // Announce, should be seperated
        System.out.printf("%-15s : %s\n", name, c.toString());
        notify();
    }

    /**
     * Sau moi luot dau, so diem se duoc tinh theo luat quy dinh truoc
     *
     * @return so diem cua nguoi choi
     */
    public int getScore() {
        return score;
    }

    /**
     * Tra ve so van thang cua nguoi choi
     *
     * @return van thang
     */
    public int getWins() {
        return wins;
    }

    public String showScore() {
        String result = "";
        for (Object o : this.get(SCSTR)) {
            result += String.format("%10s", o);
        }
        System.out.printf("%s %20s %3d\n", result, "Score: ", score);
        result += String.format("\nWins = %d", wins);
        result += String.format("\nTotal score: %3d\n", score);
        return result;
    }

    public String showChoice() {
        String result = "";
        for (Object o : this.get(CHOICESTR)) {
            result += String.format("%10s", Choice.parseInt((Integer)o));
        }
        result += "\n";
        System.out.println(result);
        return result;
    }
    
    /**
     * Lay lai ket qua cua n van truoc
     *
     * @param n
     */
    public void getBack(int n) {

        while (!this.isEmpty() && n-- != 0) {
            for (List o : this.values()) {
                if (!o.isEmpty()) {
                    if (o.equals(this.get(SCSTR))) {
                        score -= computeScore(
                                (Integer) o.get(o.size() - 1));
                        if ( (Integer)o.get(o.size() -1 ) == Choice.WIN )
                            --wins;
                    }
                    o.remove(o.size() - 1);
                }
            }
        }
    }

    public void reset() {
        for (List o : this.values()) {
            o.clear();
        }
        wins = 0;
    }

    private int computeScore(int rs) {
        switch (rs) {
            case Choice.EQUAL:
                return EQUAL_SCORE;
            case Choice.WIN:
                return WIN_SCORE;
            default:  // Case lose
                return LOSE_SCORE;
        }
    }

    public static final String CHOICESTR = "choice";
    public static final String SCSTR = "score";
    public static final int WIN_SCORE = 3;
    public static final int EQUAL_SCORE = 1;
    public static final int LOSE_SCORE = 0;

}
