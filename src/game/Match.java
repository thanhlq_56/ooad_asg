package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Chua cac thong tin ve tran dau, dieu khien tran dau
 *
 * @author thanh
 */
public class Match {

    private GameController control;
    private List<PlayerResult> matchResult;
    private List<Player> players;
    private int matchStat;
    private int roundth; // round thu may dang choi

    public Match() {
        matchResult = new ArrayList<>();
        players = new ArrayList<>();
        allMatch.add(this);
        roundth = 1;
    }

    /**
     * Tao Match tu GameController
     *
     * @param gc
     * @return
     */
    public static Match controllerCreate(GameController gc) {
        Match result = new Match();
        result.control = gc;
        System.out.println("Match.controllerCreate Ok");
        return result;
    }

    /**
     * Bat dau chay game
     */
    public void startGame() {
//        Etc.prepare(control);
        reset();
        Combat.NRounds(this);
        Utils.writePlayersChoice(getPlayers());
    }

    public synchronized void endGame() {
        System.out.println("The game is finished..............");
        matchStat = TERMINATED;
        Collections.sort(players);
        notify();
    }

    /**
     * Reset lai match: reset ket qua cua nguoi choi. Giu nguyen luat cu
     */
    public void reset() {
        for (PlayerResult pr : matchResult) {
            pr.reset();
        }
        matchStat = RUNNING;
        control.reset();
        roundth = 1;
    }

    /**
     * Neu chua co du nguoi choi thi se them Dung qua ham addPlayer
     */
    public void fillPlayer() {
        int aip = 0;
        for (int maxp = control.getMaxPlayer(), i = players.size();
                i < maxp; ++i) {
            Player p = Player.createAIPlayer();
            addPlayer(p);
            ++aip;
        }
        validatePlayer();
        if (aip == players.size()) {
            autoMatch = true;
        }
    }

    private void validatePlayer() {
        while (players.size() > control.getMaxPlayer()) {
            players.remove(players.size() - 1);
        }
    }

    public void addPlayer(Player p) {
        players.add(p);
        matchResult.add(p.getTableResult());
    }

    ////////////////// GET - SET ////////////////
    public List<PlayerResult> getGameResult() {
        return matchResult;
    }

    public GameController getController() {
        return control;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public int status() {
        return matchStat;
    }

    public void setStat(int st) {
        matchStat = st;
    }

    public String getName() {
        return "WTF";
    }

    public int getRoundth() {
        return roundth;
    }

    public void nextRound() {
        if (roundth < control.getNumber()) {
            ++roundth;
        }
    }

    ////////////////////////////
    // Trang thai tran dau
    public static final int RUNNING = 2;
    public static final int PAUSE = 1;
    public static final int TERMINATED = 0;
    public static List<Match> allMatch = new ArrayList<>();
    public boolean autoMatch;

}
