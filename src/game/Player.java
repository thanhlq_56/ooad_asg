/*
 * @project: ott
 * @author: Team JS
 */
package game;

import java.util.List;
import java.util.Scanner;

/**
 * Luu tru thong tin ve nguoi choi
 */
public class Player implements Comparable<Player> {

    private String name;
    private Brain brain;
    private Boolean auto;
    private PlayerResult result;
    private Choice lastHand;

    // Tao nguoi cho moi
    public Player(String n, boolean ai) {
        name = n;
        result = new PlayerResult(this);
        if (ai) {
            auto = true;
            brain = new Brain(Utils.fsrc, this);
        } else {
            auto = false;
        }
        numberOfPlayer++;
    }

    /**
     * tao nguoi choi don gian
     */
    public Player() {
        this("Player " + ++numberOfPlayer, false);
    }

    /**
     * Tao nguoi choi tu dong
     *
     * @return
     */
    public static Player createAIPlayer() {
        Player np = new Player("Player " + ++numberOfPlayer, true);
        return np;
    }

    /**
     * Tao nguoi choi tu input
     *
     * @return
     */
    public static Player createByConsole() {
        System.out.print("New Player's name: ");
        String n = new Scanner(System.in).nextLine();
        if (n.startsWith("ai")) {
            return createAIPlayer();
        } else {
            return new Player(n, false);
        }
    }

    /**
     * Cap nhat ket qua cho brain -> lay' co so tinh toan nuoc di tiep theo
     */
    public void updateBrain() {
        if (!isAuto()) {
            return;
        }
        List<Player> lUtility = Utils.scanPlayer(this).getPlayers();
        lUtility.stream().filter((p) -> (p != this)).forEach((p) -> {
            brain.addEnemyChoice(p.lastHand.ordinal());
        });
    }

    /**
     * Nguoi choi dua ra lua chon
     *
     * @return lua chon
     */
    public synchronized Choice pick() {
        if (isAuto()) {
            lastHand = Choice.parseInt(brain.getDecision());
            System.out.println("Player " + this + " has picked " + lastHand);
        } else {
            if (!GUI) {
                new Thread(() -> Console.getInPut(this)).start();
            }
        }
        try {
//            System.out.println(isAuto() + " is waiting");
            this.notify();
            if (!isAuto() || GUI) {
                this.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Test code
        System.out.println("has picked: " + lastHand);
        // end test
        return lastHand;
    }

    public synchronized void wakeUp() {
        this.notifyAll();
    }

    @Override
    public int compareTo(Player o) {
        return this.result.getScore() - o.result.getScore();
    }

    //////////////SET - GET ////////////////
    public void updateResult(int rs) {
        result.updateRS(rs, lastHand);
    }

    public String getName() {
        return name;
    }

    public PlayerResult getTableResult() {
        return result;
    }

    public boolean isAuto() {
        return auto && (brain != null);
    }

    public void setHand(Choice c) {
        lastHand = c;
    }

    public void setName(String s) {
        name = s;
    }

    public Brain getBrain() {
        return brain;
    }

    public Choice getHand() {
        return lastHand;
    }
    //////////////////////////////////////////////
    private static int numberOfPlayer = 0;
    public static boolean GUI;
}
