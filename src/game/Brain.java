package game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Lop cai dat, dieu khien nguoi choi tu dong
 */
public class Brain {

    // Data
    private ArrayList<Integer> enemyChoices;
    private List<Integer> expect;
    private int ch;
    private int stg; // Strategy -- lua chon chien thuat phu hop
    private Player own;
    private BufferedReader br;

    /**
     * Khoi tao doi tuong, lay du lieu thong ke tu file src
     *
     * @param src
     */
    public Brain(String src, Player p) {
        this(p);
        if (loadData(src)) {
            stg = PATTERN_STG;
        }
    }

    public Brain(Player p) {
        enemyChoices = new ArrayList<>();
        expect = new ArrayList<>();
        stg = RANDOM_STG;
        own = p;
    }

    /**
     * Lay du lieu de lam co so cho tinh toan nuoc di tiep theo Du lieu se duoc
     * add truc tiep vao list enemy
     *
     * @param src Duong dan file du lieu
     */
    private boolean loadData(String src) {
        try {
            Scanner sc = new Scanner(new File(src));
            int c;
            do {
                try {
                    c = sc.nextInt();
                    enemyChoices.add(c);
                } catch (NoSuchElementException n) {
                    sc.close();
                    break;
                }
            } while (true);
        } catch (FileNotFoundException e) {
            System.err.println("No input data!");
            return false;
        }
        return true;
    }

    /**
     * Testing code
     */
    public void shower() {
        for (Integer i : enemyChoices) {
            System.out.print(i);
        }
        System.out.println();
        for (Integer i : expect) {
            System.out.print(i);
        }
        System.out.println();
    }

    /**
     * Tao so nguyen < 3 & >= 0
     */
    private int rd() {
        return (int) (Math.random() * 3);
    }

    /**
     * Ham chinh cua brain - dua ra nuoc di tiep theo dua vao nuoc di cua nguoi
     * choi --Con rat banh trach--
     */
    private void patternIdea() {
        // Chua co can cu
        if (enemyChoices == null) {
            stg = RANDOM_STG;
            return;
        }

        // Neu van truoc khong thang thi build lai pattern
        int seed;
        try {
            seed = enemyChoices.get(0);
        } catch (Exception e) {
            seed = 0;
//            e.printStackTrace();
        }
        try {
            if (!Choice.beat(ch, enemyChoices.get(enemyChoices.size() - 1))) {
                buildPattern(seed);
//                System.out.println("Built!");
            }
        } catch (ArrayIndexOutOfBoundsException ae) {
        }

        // Ok! it worked - ^^
        if (!expect.isEmpty()) {
            ch = Choice.enemyInt(expect.get((enemyChoices.indexOf(seed)
                    + enemyChoices.size())
                    % expect.size()));
        } else {
            stg = RANDOM_STG;
        }

    }

    /**
     * Tao mau nuoc di cua nguoi choi
     *
     * @param li
     * @param seed
     */
    private void buildPattern(int seed) {

        List<Integer> buff = new ArrayList<>(
                enemyChoices.subList(enemyChoices.indexOf(seed)
                        + expect.size(), enemyChoices.size()));
        int head = 1 + buff.subList(1, buff.size()).indexOf(seed);
        if (head != -1) {
            expect.addAll(buff.subList(0, head));
        }
    }

    /**
     * May dua ra quyet dinh sau nghi
     *
     * @return lua chon ( 1 - 3 )
     */
    public int getDecision() {
//        if ( Utils.scanPlayer(own).autoMatch )
//            stg = RANDOM_STG;
        switch (stg) {
            case PATTERN_STG:
                patternIdea();
            case RANDOM_STG:
                ch = rd();
        }
        return ch;
    }

    ///////////////// SET _ GET /////////////////
    public void addEnemyChoice(int c) {
        enemyChoices.add(c);
    }
    ////////////////////////////////////////////

    private static final int RANDOM_STG = 0;
    private static final int PATTERN_STG = 1;
}
