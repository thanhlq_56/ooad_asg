/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author thanh
 */
public class ReachNPoint extends GameController {

    private int roundth;

    public ReachNPoint(int n, int p) {
        number = n;
        maxPlayer = p;
    }

    @Override
    public boolean isDone() {
        return roundth >= number;
    }

    @Override
    public void update() {
        roundth++;
    }

    public int getRoundth() {
        return roundth;
    }

    @Override
    public void reset() {
        roundth = 0;
    }
    
    public String gameDes(){
        return GAMEDES;
    }
    
    public static final String GAMEDES = "Who reach the peak fist?";
}
