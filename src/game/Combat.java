package game;

import java.util.ArrayList;
import java.util.List;

/**
 * Lop xu li van choi cua tran dau
 *
 * @author thanh
 */
public final class Combat {

    /**
     * Tran dau cua 1 nhom nguoi choi Choi den khi het N rounds
     *
     * @param pls Nguoi choi tham gia
     * @param n So van dau
     *
     * < Unused >
     */
    public static void NRounds(List<Player> pls, int n) {
        if (pls.isEmpty()) {
            return;
        }

        // Luu tam ket qua
        List<Player> wins = new ArrayList<>();
        List<Player> loses = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            wins.clear();
            loses.clear();
            fight(pls, wins, loses);

            if (wins.isEmpty()) {
                judge(pls, Choice.EQUAL);
            } else {
                judge(wins, Choice.WIN);
                judge(loses, Choice.LOSE);
            }

        }
    }

    /**
     * Choi theo luat cua tran dau Tran dau dieu khien van choi bang control (
     * luat choi )
     *
     * @param m Tran dau
     */
    public static void NRounds(Match m) {
        if (m.status() == Match.TERMINATED) {
            return;
        }
        // Luu tam ket qua
        List<Player> wins = new ArrayList<>();
        List<Player> loses = new ArrayList<>();
        List<Player> pls = m.getPlayers();

        while (!m.getController().isDone()) {
            if (m.status() == Match.TERMINATED) {
                unjudge(pls, 1);
                break;
            }
            m.nextRound();
            wins.clear();
            loses.clear();
            m.getController().update();
            fight(pls, wins, loses);
            
            if (wins.isEmpty()) {
                judge(pls, Choice.EQUAL);
            } else {
                judge(wins, Choice.WIN);
                judge(loses, Choice.LOSE);
            }
            for (Player p : pls) {
                p.updateBrain();        // bao cho AI biet, de lay can cu cho tinh toan
            }
            synchronized (ENROUND_OB) {
                ENROUND_OB.notifyAll();
            }
        }
        m.endGame();
    }

    /**
     * Combat trong 1 round Dua nguoi choi lua chon -> Quyet dinh ben thang,
     * thua
     *
     * @param pls Nguoi choi trong round
     * @param wins Nhung nguoi thang
     * @param loses Nhom nguoi thua
     */
    public static void fight(List<Player> pls,
            List<Player> wins,
            List<Player> loses) {
        // init
        int max = pls.size();
        List<Player> rockrs = new ArrayList<>();
        List<Player> scirs = new ArrayList<>();
        List<Player> papers = new ArrayList<>();
        List<Player> unfair = new ArrayList<>();

        // end initialization
        for (Player pl : pls) {
            System.out.println(pl);
            switch (pl.pick()) {
                case PAPER:
                    papers.add(pl);
                    break;
                case ROCK:
                    rockrs.add(pl);
                    break;
                case SCISSORS:
                    scirs.add(pl);
                    break;
            }
        }

        // Xet player va ketqua
        // Nguoi choi unfair luon bi loai (unfair la ket qua ko hop le)
        loses.addAll(unfair);

        /**
         * *Truong hop hoa***
         */
        // Tat ca chon giong nhau
        if (scirs.size() == max
                || rockrs.size() == max
                || papers.size() == max) {
            return;
        }

        // 3 phuong an deu duoc chon
        if (!scirs.isEmpty()
                && !papers.isEmpty()
                && !rockrs.isEmpty()) {
            return;
        }

        /**
         * *Truong hop co thang thua*****
         */
        if (scirs.isEmpty()) {
            // Co ai do ko chon va con lai thi chon 1 loai
            if (!(papers.isEmpty()
                    || rockrs.isEmpty())) {
                wins.addAll(papers);
                loses.addAll(rockrs);
            }
            return;
        }

        if (papers.isEmpty()) {
            // Co ai do ko chon va con lai thi chon 1 loai
            if (!(rockrs.isEmpty()
                    || scirs.isEmpty())) {
                wins.addAll(rockrs);
                loses.addAll(scirs);
            }
            return;
        }

        if (rockrs.isEmpty()) {
            // Co ai do ko chon va con lai thi chon 1 loai
            if (!(scirs.isEmpty()
                    || papers.isEmpty())) {
                wins.addAll(scirs);
                loses.addAll(papers);
            }
            return;
        }

    }

    /**
     * Ham tinh diem, luu ket qua
     */
    private static void judge(List<Player> pls, int rs) {
        for (Player pl : pls) {
            pl.updateResult(rs);
        }
    }

    /**
     * Go lai diem da luu ?? Cha hieu tai sao co ham nay :)
     *
     * @param pls
     * @param n
     */
    private static void unjudge(List<Player> pls, int n) {
        for (Player pl : pls) {
            pl.getTableResult().getBack(n);
        }
    }

    public static final Object ENROUND_OB = new Object();
}
