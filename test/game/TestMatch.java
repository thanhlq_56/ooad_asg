/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.math.MathContext;

/**
 *
 * @author thanh
 */
public class TestMatch {

    public static void main(String[] args) {
        NRoundController nrc = new NRoundController(3, 5);
        Match m = Match.controllerCreate(nrc);
        m.getPlayers().add(new Player("Thanh"));
        m.fillPlayer();
        m.startGame();
        System.out.println(m.status());
        Utils.showPlayersResult(m.getPlayers());
    }
}
