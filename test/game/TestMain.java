package game;

import game.*;

public class TestMain {
    
    
    
    
    
    public static void main(String[] args) {
        NRoundController nrc = new NRoundController(30, 2);
        Match m = Match.controllerCreate(nrc);
        m.fillPlayer();
        m.setStat(Match.RUNNING);
        m.startGame();
        Utils.showPlayersResult(m.getPlayers());
    }
}
